# Simple Hunt Showdown Kit Randomizer

- Generates a kit including the following:
	- 2 weapons
	- 4 tools (won't include duplicates)
	- 4 consumables (may include duplicates)


- I plan to make a more complex version later with support for the following:
	- User Options:
		- Special Ammo
		- Weapon Size Selection (2 medium weapons, 2 small &c.)
		- Weapon Bullet Type (compact, medium, long, special)
		- Weapon Count (1 or 2)
		- Tool Count (up to 4)
		- Consumable Count (up to 4)
		- Random Trait(s)



