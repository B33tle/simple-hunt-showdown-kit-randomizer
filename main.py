'''
Hunt Showdown Kit Randomizer
-Produces random kits including weapons, tools and consumables
'''

# Imports ================================
from hunt_item_names import small_weapons, medium_weapons, large_weapons, tool_names, consumable_names
from random import randint
import random


# FUNCTIONS ===================================
def print_all_weapons():
    # Prints All Small, Medium & Large Weapon Names
    for thing in small_weapons, medium_weapons, large_weapons:
        bulleted(thing)

# This is easier to do with numpy.random.choice
# But random is included in the standard library so this makes more sense
# Choice returns a list of n-random-choices from thing
choice = lambda thing, n: random.choice(thing) if n == 1 else [random.choice(thing) for i in range(n)]

def get_random_weapons():
    '''
    Returns one of the following:
    * 2 small weapons
    * 2 medium weapons
    * 1 Large & 1 Small Weapon
    * 1 Medium & 1 Small Weapon
    '''

    r = randint(0,4)
    
    # 2 Small Weapons
    if r == 0:
        return choice(small_weapons, 2)
    
    # 2 Medium Weapons
    if r == 1:
        return choice(medium_weapons, 2)
    
    # 1 Large & 1 Small Weapon
    if r == 2:
        x = choice(large_weapons, 1)
        y = choice(small_weapons, 1)
        return x, y
    
    # 1 Medium & 1 Small Weapon
    else:
        x = choice(medium_weapons, 1)
        y = choice(small_weapons, 1)
        return x, y
def get_random_tools(n = 4):
    '''
    Returns some number of tools with no repeats (default is 4)
    '''

    # Repeats until set of tools without repeats is found
    while 1:
        # May generate repeats
        tools = choice(tool_names, n)

        # If we get the desired number of tools, return them
        if len(set(tools)) == len(tools):
            return tools

def get_random_consumables(n = 4):
    '''
    Returns some number of consumables, allowing for repeats
    '''
    return choice(consumable_names, n)

def bulleted(title, arr):
    '''Prints each item in an array/list with a preceding hyphen'''
    emphasize = lambda s: print('=' * 10, s, '=' * 10) # prints string with EMPHASIS
    emphasize(title)
    for i in arr:
        print(f"-{i}")
    print("")

# MAIN ===========================================
if __name__ == '__main__':
    
    # GET RANDOM KIT ==========================
    weapons = get_random_weapons()
    tools = get_random_tools()
    consumables = get_random_consumables()
    
    # PRINT KIT ===============================
    bulleted('WEAPONS',weapons)
    bulleted('TOOLS',tools)
    bulleted('CONSUMABLES',consumables)
        

